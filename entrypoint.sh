#!/bin/bash
	echo "iniciando entorno"
	project_path=/home/project/$PROJECT_NAME
	export PATH=$PATH:/sbin:/bin:/usr/bin:/usr/local/bin:/usr/sbin

    sudo chown theia /home/theia/.theia -R
	# env
	# tail -f /dev/null
	credenciales=""
	if [ "$GIT_USERNAME" != "" ]; then
		credenciales="${GIT_USERNAME}:${GIT_PASSWORD}@"
	fi

	git config --global user.name "$GIT_FULLNAME"
	git config --global user.email "$GIT_EMAIL"
	git config --global --add push.pushOption merge_request.create
	git config --global --add push.pushOption merge_request.remove_source_branch

	if [ "$GIT_REPO" != "" ]; then

		project_path=$(echo "$GIT_REPO" | sed -e "s/^https:\/\///g")
		project_path=$(echo "$project_path" | sed -e "s/\.git$//g")
		project_path=/home/project/src/$project_path

		# descargo el reposiotrio
		url=${GIT_REPO/"https://"/"https://${credenciales}"}
		git clone "$url" $project_path
		cd $project_path
		git submodule init
		git submodule update
		# git submodule foreach git checkout master
	else
		cd /home/project/$PROJECT_NAME

		# obtengo el path desde el repositorio
		project_path=$(git remote get-url --all origin | sed -r 's/https[:/]+(.+@|)(.+)\.git$/\2/')
		project_path=$(echo $project_path | sed -r 's/git@(.+):(.+)\.git$/\1\/\2/')
		project_path=/home/project/src/$project_path
		mkdir -p $(dirname $project_path)
		echo $project_path
		if [ ! -d "$project_path" ]; then
			ln -s /home/project/$PROJECT_NAME $project_path
		fi
	fi

	if [ "$GIT_REPO2" != "" ]; then
		project_path2=$(echo "$GIT_REPO2" | sed -e "s/^https:\/\///g")
		project_path2=$(echo "$project_path2" | sed -e "s/\.git$//g")
		project_path2=/home/project/src/$project_path2

		# descargo el reposiotrio
		url=${GIT_REPO2/"https://"/"https://${credenciales}"}
		git clone "$url" $project_path2
	fi

	ls -la $(dirname $project_path)

	# tail -f /dev/null
	cd /home/theia

	if [ ! -f  "/home/theia/certs/server.crt" ]; then
		echo "Generando Certificado Autofirmado"
		openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certs/server.key -out  certs/server.crt -batch
	fi


	if [ -f "$project_path/.theia-workspace" ]; then
		node /home/theia/src-gen/backend/main.js "$project_path/.theia-workspace" --hostname=0.0.0.0 --ssl --cert /home/theia/certs/server.crt --certkey /home/theia/certs/server.key
	else 
		node /home/theia/src-gen/backend/main.js $project_path --hostname=0.0.0.0 --ssl --cert /home/theia/certs/server.crt --certkey /home/theia/certs/server.key
	fi



